using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Averor.SimpleRabbitmqConsumer
{
    class Consumer
    {
        private IConnection connection;
        private Dictionary<string, IModel> channels = new Dictionary<string, IModel>();

        public void connect(string uri)
        {
            try {
                ConnectionFactory factory = new ConnectionFactory();
                factory.Uri = new System.Uri(uri);
                factory.AutomaticRecoveryEnabled = true;
                this.connection = factory.CreateConnection();
            } catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException e) {
                throw new ConnectionException("Could connect to the host.", e.Message);
            } catch (System.Exception e) {
                throw new ConnectionException("Connection exception.", e.Message);
            }
        }

        public void consume(string queueName, Handler handler)
        {
            IModel channel = this.connection.CreateModel();
            this.channels[queueName] = channel;

            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);

            consumer.Received += (ch, ea) => {

                handler.handle(
                    System.Text.Encoding.UTF8.GetString(ea.Body)
                );

                channel.BasicAck(ea.DeliveryTag, false);
            };

            String consumerTag = channel.BasicConsume(queueName, false, consumer);
        }

        public void close()
        {
            foreach(KeyValuePair<string, IModel> entry in this.channels) {
                entry.Value.Close();
            }
            this.connection.Close();
        }
    }

    public class ConnectionException : System.Exception
    {
        public string Details { get; private set;}

        public ConnectionException(string message, string details) : base(message)
        {
            this.Details = details;
        }

        public ConnectionException(string message, string details, System.Exception inner) : base(message)
        {
            this.Details = details;
        }
    }
}
