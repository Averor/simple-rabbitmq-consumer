﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Averor.SimpleRabbitmqConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            string RabbitMQUri = Environment.GetEnvironmentVariable("AMQP_URI");

            string configFile = "config.json";

            if (args.Length == 1) {
                configFile = args[0];
            }

            try {

                IConfigurationBuilder builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile(path: configFile, optional: false, reloadOnChange: true);

                IConfigurationRoot Configuration = builder.Build();

                Dictionary<string, string> handlers = Configuration.GetSection("handlers").GetChildren().ToDictionary(x => x.Key, x => x.Value);

                Consumer consumer = new Consumer();

                consumer.connect(RabbitMQUri);

                foreach(KeyValuePair<string, string> handler in handlers) {
                    consumer.consume(
                        handler.Key, 
                        new Handler(handler.Value)
                    );
                }

                Console.Read();

            } catch (System.IO.FileNotFoundException e) {
                Console.WriteLine($"[x] FileNotFoundException {e.Message}");
                System.Environment.Exit(101);
            } catch (ConnectionException e) {
                Console.WriteLine($"[x] ConnectionException {e.Message} ({e.Details})");
                System.Environment.Exit(102);
            } catch (System.Exception e) {
                Console.WriteLine($"[x] Exception {e.Message} {e.StackTrace}");
                System.Environment.Exit(1);
            }
        }
    }
}
