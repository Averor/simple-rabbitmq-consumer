using System;
using System.Diagnostics;

namespace Averor.SimpleRabbitmqConsumer
{
    class BashExecute
    {
        public static int execute(string file)
        {
            Process p = createProcess(file);
            
            p.Start();
            p.WaitForExit();

            return p.ExitCode;
        }

        public static int execute(string file, string args)
        {
            Process p = createProcess(file);

            p.StartInfo.Arguments = args;
            
            p.Start();
            p.WaitForExit();

            return p.ExitCode;
        }

        private static Process createProcess(string file)
        {
            Process p = new Process();

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = file;

            return p;
        }
    }
}
