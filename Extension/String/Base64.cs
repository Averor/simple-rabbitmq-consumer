namespace Averor.SimpleRabbitmqConsumer.Extension.String
{
    public static class Base64
    {
        public static string Base64Encode(this System.Text.Encoding encoding, string text)
        {
            if (text == null) {
                return null;
            }

            byte[] textAsBytes = encoding.GetBytes(text);
            return System.Convert.ToBase64String(textAsBytes);
        }

        public static string Base64Decode(this System.Text.Encoding encoding, string encodedText)
        {
            if (encodedText == null) {
                return null;
            }

            byte[] textAsBytes = System.Convert.FromBase64String(encodedText);
            return encoding.GetString(textAsBytes);
        }
    }
}
