using System;
using System.Linq;

using Averor.SimpleRabbitmqConsumer.Extension.String;

namespace Averor.SimpleRabbitmqConsumer
{
    class Handler
    {
        public string command { get; private set; }
        public string args { get; private set; }

        public Handler(string command)
        {
            string[] parts = command.Split(" ");

            if (parts.Length == 0) {
                this.command = "";
                this.args = "";
                return;
            }

            this.command = parts[0];
            this.args = String.Join(" ", parts.Skip(1).ToArray());
        }

        public int handle(string message)
        {
            string parsedArgs = "";

            if (this.args.Contains("{message}")) {
                parsedArgs = this.args.Replace("{message}", message);
            }
            else if(this.args.Contains("{messageBase64}")) {
                parsedArgs = this.args.Replace("{messageBase64}", System.Text.Encoding.UTF8.Base64Encode(message));
            }
            else {
                parsedArgs = this.args;
            }

            //int exitCode = new Int16();

            if (parsedArgs.Length > 0) {
                //exitCode = 
                return BashExecute.execute(this.command, parsedArgs);
            } else {
                //exitCode = 
                return BashExecute.execute(this.command);
            }

            //Console.WriteLine($"RabbitMQ::Consumer::cmd output::{exitCode}");

            // return exitCode;
        }
    }

    public class HandlerException : System.Exception
    {
        public string Details { get; private set;}

        public HandlerException(string message, string details) : base(message)
        {
            this.Details = details;
        }

        public HandlerException(string message, string details, System.Exception inner) : base(message)
        {
            this.Details = details;
        }
    }
}
