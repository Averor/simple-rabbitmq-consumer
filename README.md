### Description

Simple console dotnet core RabbitMQ amqp queue consumer, written in C#.
Fetches messages from queue and forwards them, as string, to the specified handler (shell command)

### Build & Publish
TODO, but as for now it's just

    dotnet publish -c release --runtime linux-x64 --framework netcoreapp2.1 -o obj/Final

### Configuration
    AMQP_URI=amqp://user:pass@host:port/vhost

    env var, containing RabbitMW connection uri, needs to be available (i.e. via export)
    vhost name starting with slash must be uri decoded (ie /SomeVHost => %2FSomeVHost => amqp://user:pass@host:port/%2FSomeVHost)

config.json file should be either placed in same folder as executable
or execute app providing path to it as first argument


    {
        "handlers": {
            "first_of_my_queues": "/home/user/some_script.sh {message}",
            "second_of_my_queues": "/home/user/some_other_script.sh -m {messageBase64} -u guest"
        }
    }

* **{message}** - message string that will be forwarded AS-RECEIVED from queue
* **{messageBase64}** - message string, received from queue, will be encoded with Base64
* (Note that only one command per queue can be provided)

### To Do
* Further reading on https://github.com/dotnet/core/blob/master/samples/linker-instructions.md to shrink self-contained app.
* Further reading on https://www.rabbitmq.com/dotnet-api-guide.html to enrich this funny app ;)